FROM alpine:3.15 as source

ARG ONCALL_COMMIT_HASH
ARG ONCALL_SOURCE_URL
RUN apk update && apk add --no-cache wget && wget -O archive.zip ${ONCALL_SOURCE_URL} && \
    unzip archive.zip && rm archive.zip && mv oncall-${ONCALL_COMMIT_HASH} /home/oncall
WORKDIR /home/oncall
# Fix broken file permissions from the zip file (direcotries are group and world writable)
RUN chmod -R og-w .
# patch source to configure sender rpc log file to a writeable location    
RUN  sed -i "s/^initializedfile.*/initializedfile = '\/tmp\/db_initialized'/" ops/entrypoint.py

FROM ubuntu:20.04

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::=--force-confold dist-upgrade \
    && DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::=--force-confold install libffi-dev libsasl2-dev python3-dev \
        libldap2-dev libssl-dev python3-pip python3-setuptools python3-venv \
        mysql-client uwsgi uwsgi-plugin-python3 nginx \
    && rm -rf /var/cache/apt/archives/*

WORKDIR /home/oncall

COPY --from=source /home/oncall/src source/src
COPY --from=source /home/oncall/setup.py source/setup.py
COPY --from=source /home/oncall/MANIFEST.in /home/oncall/README.md /home/oncall/LICENSE source/

RUN python3 -m venv /home/oncall/env && \
    /bin/bash -c 'source /home/oncall/env/bin/activate && cd /home/oncall/source && pip install --no-cache-dir wheel && pip install --no-cache-dir .'

COPY --from=source /home/oncall/ops/daemons daemons
COPY --from=source /home/oncall/ops/daemons/uwsgi-docker.yaml daemons/uwsgi.yaml
COPY --from=source /home/oncall/db db
COPY --from=source /home/oncall/configs config
COPY --from=source /home/oncall/ops/entrypoint.py entrypoint.py

RUN useradd -m -s /bin/bash oncall && \
    chown -R oncall:oncall /var/log/nginx /var/lib/nginx && \
    mkdir -p /home/oncall/var/log/uwsgi /home/oncall/var/log/nginx /home/oncall/var/run /home/oncall/env/lib/python3.8/site-packages/oncall/ui/static/.webassets-cache /home/oncall/env/lib/python3.8/site-packages/oncall/ui/static/bundles && \
    chown -R oncall:oncall /home/oncall/var/log/uwsgi /home/oncall/var/log/nginx /home/oncall/var/run /home/oncall/env/lib/python3.8/site-packages/oncall/ui/static/.webassets-cache /home/oncall/env/lib/python3.8/site-packages/oncall/ui/static/bundles

EXPOSE 8080

USER oncall

# pre-build static assets
RUN  /bin/bash -c 'source /home/oncall/env/bin/activate && build_assets build && build_assets check'

CMD ["bash", "-c", "source /home/oncall/env/bin/activate && exec python -u /home/oncall/entrypoint.py"]
