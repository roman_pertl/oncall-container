ONCALL_COMMIT_HASH=262af21037ab7e901a8ba8d17c86e2ba4f7fe56e
ONCALL_SOURCE_URL=https://github.com/linkedin/oncall/archive/${ONCALL_COMMIT_HASH}.zip

build:
	podman build --tag oncall:latest \
		--build-arg ONCALL_COMMIT_HASH=${ONCALL_COMMIT_HASH} \
		--build-arg ONCALL_SOURCE_URL=${ONCALL_SOURCE_URL} \
		--label org.opencontainers.image.created="$(date)" \
		--label org.opencontainers.image.version="${ONCALL_COMMIT_HASH}" \
		--label org.opencontainers.image.revision="${ONCALL_COMMIT_HASH}" \
		-f Containerfile . 
kics:
	podman run --rm -v ${PWD}:/path checkmarx/kics scan --no-progress -p "/path" -o "/path/"
